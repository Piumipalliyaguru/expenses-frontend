import React, { Component } from "react";
// import { login } from "../../util/APIUtils.js";
import "./Login.css";
import LoginForm from "./LoginForm.js";
import { Form } from "antd";

class Login extends Component {
  render() {
    const AntWrappedLoginForm = Form.create()(LoginForm);
    return (
      <div className="login-container">
        <h1 className="page-title">Login</h1>
        <div className="login-content">
          <AntWrappedLoginForm onLogin={this.props.onLogin} />
        </div>
      </div>
    );
  }
}

export default Login;
